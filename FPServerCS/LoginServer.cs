﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;

namespace FPServerCS
{
    class LoginServer
    {
        public Socket loginSocket;
        private IPEndPoint ipend;
        private SocketAsyncEventArgs acptAsync;
        private SocketAsyncEventArgs recvAsync;
        private SocketAsyncEventArgs sendAsync;
        private List<Socket> client_list;
        private float ver = 0.1f;
        public LoginServer(int port)
        {
            init();
            loginSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
            ipend = new IPEndPoint(IPAddress.Any, port);
            loginSocket.Bind(ipend);
            loginSocket.Listen(30);
            Console.WriteLine("Login Server listened on TCP {0}",port);
            loginSocket.AcceptAsync(acptAsync);
        }
        private void init()
        {
            acptAsync = new SocketAsyncEventArgs();
            acptAsync.Completed += new EventHandler<SocketAsyncEventArgs>(acptComplete);
            recvAsync = new SocketAsyncEventArgs();
            recvAsync.Completed += new EventHandler<SocketAsyncEventArgs>(recvComplete);
            sendAsync = new SocketAsyncEventArgs();
            sendAsync.Completed += new EventHandler<SocketAsyncEventArgs>(sendComplete);
            client_list = new List<Socket>();
        }
        public void end()
        {
            loginSocket.Close();
        }
        private void acptComplete(object s,SocketAsyncEventArgs e)
        {
            Socket client = e.AcceptSocket;
            if (client != null)
            {
                client_list.Add(client);
                SocketAsyncEventArgs args = new SocketAsyncEventArgs();
                byte[] buffer = new byte[2048];
                args.SetBuffer(buffer, 0, buffer.Length);
                args.UserToken = client;
                args.Completed += new EventHandler<SocketAsyncEventArgs>(recvComplete);
                client.ReceiveAsync(args);
            }
            e.AcceptSocket = null;
            loginSocket.AcceptAsync(acptAsync);
        }
        private void recvComplete(object s, SocketAsyncEventArgs e)
        {
            Socket client = (Socket)s;
            if(client.Connected && e.BytesTransferred>0)
            {
                byte[] buffer = e.Buffer;
                string data = Encoding.UTF8.GetString(buffer);
                Packet.Packet recv = JsonConvert.DeserializeObject<Packet.Packet>(data);
                Packet.Packet sendP;
                switch (recv.packetType)
                {
                    case (int)Packet.PacketType.OP_LoginRequest: sendP=GeneratePacket((int)Packet.PacketType.OP_LoginResult,false,checkValid(recv.body)); break;
                    case (int)Packet.PacketType.OP_ServerInfo: sendP = GeneratePacket((int)Packet.PacketType.OP_ServerInfo,false,getServerStatus());break;
                    default: sendP = null; break;
                }
                send(client,sendP);
                SocketAsyncEventArgs args = new SocketAsyncEventArgs();
                byte[] buffer2 = new byte[2048];
                args.SetBuffer(buffer2, 0, 2048);
                args.UserToken = client;
                args.Completed += new EventHandler<SocketAsyncEventArgs>(recvComplete);
                client.ReceiveAsync(args);
            }
        }
        private string checkValid(string data)
        {
            Packet.LoginRequest r = JsonConvert.DeserializeObject<Packet.LoginRequest>(data);
            Packet.LoginResult re;
            /*if (r.version != ver)
            {
                re = new Packet.LoginResult(1, -1,"");
            }
            else if (!r.username.Equals("admin"))
            {
                re = new Packet.LoginResult(2, -1,"");
            }
            else if (!r.passwd.Equals("1234"))
            {
                re = new Packet.LoginResult(3, -1,"");
            }
            else
            {*/
                string key = new session.Session(r.username).getKey();
                re = new Packet.LoginResult(0, 0,key);
            //}
            return JsonConvert.SerializeObject(re);
        }
        private string getServerStatus()
        {
            return JsonConvert.SerializeObject(new Packet.ServerStatus("127.0.0.1", 51001, "Test Server", (int)Packet.ServerStatusCode.OP_open));
        }
        private Packet.Packet GeneratePacket(int packetType,bool secure,string body)
        {
            return new Packet.Packet(packetType,secure,body);
        }
        private void send(Socket client,Packet.Packet p)
        {
            byte[] buffer = new byte[2048];
            buffer = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(p));
            sendAsync.SetBuffer(buffer, 0, buffer.Length);
            client.SendAsync(sendAsync);
        }
        private void sendComplete(object s,SocketAsyncEventArgs e)
        {

        }
    }
}
