﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPServerCS.Packet
{
    class LoginResult
    {
        public int msg;
        public int uid;
        public string session;
        public LoginResult(int msg,int uid,string session)
        {
            this.msg = msg;
            this.uid = uid;
            this.session = session;
        }
    }
}
