﻿namespace FPServerCS.Packet
{
    public class Packet
    {
        public int packetType;
        public bool secure;
        public string body;
        public Packet(int packetType,bool secure,string body)
        {
            this.packetType = packetType;
            this.secure = secure;
            this.body = body;
        }
    }
}
