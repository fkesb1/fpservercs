﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPServerCS.Packet
{
    class ServerStatus
    {
        public string addr;
        public int port;
        public string name;
        public int status;
        public ServerStatus(string addr,int port,string name,int status)
        {
            this.addr = addr;
            this.port = port;
            this.name = name;
            this.status = status;
        }
    }
}
