﻿namespace FPServerCS.Packet
{
    enum PacketType
    {
        OP_dummy,
        OP_ServerInfo,
        OP_ServerInfoResult,
        OP_LoginRequest,
        OP_LoginResult
    }
    enum ServerStatusCode
    {
        OP_open,
        OP_lock,
        OP_Maintain
    }
    enum Loginop
    {
        OP_Success,
        OP_NoUsername,
        OP_Wrongpw,
        OP_Unknown
    }
}