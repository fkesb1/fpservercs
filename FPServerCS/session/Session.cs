﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace FPServerCS.session
{
    class Session
    {
        public static List<Session> session=new List<Session>();
        public string key;
        public EndPoint clientEp;
        public int zone_id;
        public Session(string id)
        {
            byte[] b = Encoding.UTF8.GetBytes(id+'/'+DateTime.Now.ToString("h:mm:ss tt"));
            this.key = Convert.ToBase64String(b);
            this.zone_id = 0;
            Console.WriteLine("session key :{0}", this.key);
            session.Add(this);
        }
        public string getKey()
        {
            return this.key;
        }
    }
}
