﻿namespace FPServerCS.world.Packet
{
    class HelloWorldReply
    {
        public int zone_id;
        public int faction;
        public HelloWorldReply(int zone_id, int faction)
        {
            this.zone_id = zone_id;
            this.faction = faction;
        }
    }
}
