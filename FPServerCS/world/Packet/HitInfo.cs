﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPServerCS.world.Packet
{
    class HitInfo
    {
        public string username;
        public float damage;
        public string from;
        public int zone_id;
    }
}
