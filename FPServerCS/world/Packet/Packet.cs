﻿namespace FPServerCS.world.Packet
{
    public class Packet
    {
        public int packetType;
        public bool secure;
        public string body;
        public string session;
        public Packet(int packetType, bool secure, string body,string session)
        {
            this.packetType = packetType;
            this.secure = secure;
            this.body = body;
            this.session = session;
        }
    }
}
