﻿namespace FPServerCS.world.Packet
{
    enum Faction
    {
        US,
        RU
    }
    enum PacketType
    {
        OP_dummy,
        OP_HelloWorldMessage,
        OP_HelloWorldReply,
        OP_ZoneStatus,
        OP_ZoneStatusReply,
        OP_SessionCheck,
        OP_SessionCheckResult,
        OP_PlayerMove,
        OP_PlayerInstant,
        OP_shoot,
        OP_hit,
        OP_kill,
        OP_broadcase_msg,
        OP_zoneChat
    }
}
