﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace FPServerCS.world
{
    class WorldServer
    {
        public Socket worldSocket;
        public Socket udpSocket;
        private IPEndPoint ipend;
        private SocketAsyncEventArgs acptAsync;
        private SocketAsyncEventArgs recvAsync;
        private SocketAsyncEventArgs sendAsync;
        private SocketAsyncEventArgs udpRecvAsync;
        private SocketAsyncEventArgs hitsend;
        private SocketAsyncEventArgs sessionCheckReturn;
        private List<Socket> client_list;
        private static int cnt = 0;
        public WorldServer(int port)
        {
            init();
            worldSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
            ipend = new IPEndPoint(IPAddress.Any, port);
            worldSocket.Bind(ipend);
            worldSocket.Listen(30);
            Console.WriteLine("World Server listened on TCP {0}", port);
            worldSocket.AcceptAsync(acptAsync);
            udpSocket = new Socket(SocketType.Dgram, ProtocolType.Udp);
            udpSocket.Bind(ipend);
            Console.WriteLine("World Server listened on UDP {0}", port);
            udpSocket.ReceiveFromAsync(udpRecvAsync);
        }
        private void init()
        {
            acptAsync = new SocketAsyncEventArgs();
            acptAsync.Completed += new EventHandler<SocketAsyncEventArgs>(acptComplete);
            recvAsync = new SocketAsyncEventArgs();
            recvAsync.Completed += new EventHandler<SocketAsyncEventArgs>(recvComplete);
            sendAsync = new SocketAsyncEventArgs();
            sendAsync.Completed += new EventHandler<SocketAsyncEventArgs>(sendComplete);
            udpRecvAsync = new SocketAsyncEventArgs();
            udpRecvAsync.RemoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
            udpRecvAsync.SetBuffer(new byte[1024], 0, 1024);
            udpRecvAsync.Completed += new EventHandler<SocketAsyncEventArgs>(udpRecvCallback);
            sessionCheckReturn = new SocketAsyncEventArgs();
            client_list = new List<Socket>();
        }
        public void end()
        {
            worldSocket.Close();
            udpSocket.Close();
        }
        private void acptComplete(object s, SocketAsyncEventArgs e)
        {
            Socket client = e.AcceptSocket;
            if (client != null)
            {
                client_list.Add(client);
                SocketAsyncEventArgs args = new SocketAsyncEventArgs();
                byte[] buffer = new byte[1024];
                args.SetBuffer(buffer, 0, buffer.Length);
                args.UserToken = client;
                args.Completed += new EventHandler<SocketAsyncEventArgs>(recvComplete);
                client.ReceiveAsync(args);
            }
            e.AcceptSocket = null;
            worldSocket.AcceptAsync(acptAsync);
        }
        private void recvComplete(object s, SocketAsyncEventArgs e)
        {
            Socket client = (Socket)s;
            if (client.Connected && e.BytesTransferred > 0)
            {
                byte[] buffer = e.Buffer;
                string data = Encoding.UTF8.GetString(buffer);
                Packet.Packet recv = JsonConvert.DeserializeObject<Packet.Packet>(data);
                Packet.Packet sendP;
                switch (recv.packetType)
                {
                    case (int)Packet.PacketType.OP_HelloWorldMessage: sendP = GeneratePacket((int)Packet.PacketType.OP_HelloWorldReply, false, setZone(recv.body),""); break;
                    case (int)Packet.PacketType.OP_ZoneStatus: sendP = GeneratePacket((int)Packet.PacketType.OP_ZoneStatusReply, false, getZoneStatus(0),""); break;
                    default: sendP = null; break;
                }
                send(client, sendP);
                SocketAsyncEventArgs args = new SocketAsyncEventArgs();
                byte[] buffer2 = new byte[1024];
                args.SetBuffer(buffer2, 0, buffer2.Length);
                args.UserToken = client;
                args.Completed += new EventHandler<SocketAsyncEventArgs>(recvComplete);
                client.ReceiveAsync(args);
            }
        }
        
        private Packet.Packet GeneratePacket(int packetType, bool secure, string body,string session)
        {
            return new Packet.Packet(packetType, secure, body,session);
        }
        private void send(Socket client, Packet.Packet p)
        {
            byte[] buffer = new byte[1024];
            buffer = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(p));
            sendAsync.SetBuffer(buffer, 0, buffer.Length);
            client.SendAsync(sendAsync);
        }
        private void sendComplete(object s, SocketAsyncEventArgs e)
        {

        }

        private string setZone(string data)
        {
            Packet.HelloWorld h = JsonConvert.DeserializeObject<Packet.HelloWorld>(data);
            Packet.HelloWorldReply r = new Packet.HelloWorldReply(1, h.faction);
            return JsonConvert.SerializeObject(r);
        }
        private string getZoneStatus(int zone_id)
        {
            return "";
            //zone.clean_room_area test = new zone.clean_room_area(JsonConvert.SerializeObject(new zone.clean_room.test_facilityA()), JsonConvert.SerializeObject(new zone.clean_room.test_facilityB()));
            //return JsonConvert.SerializeObject(test);
        }

        private void udpRecvCallback(object s,SocketAsyncEventArgs e)
        {
            if (e.BytesTransferred > 0)
            {
                try
                {
                    string data = Encoding.UTF8.GetString(e.Buffer);
                    world.Packet.Packet p = JsonConvert.DeserializeObject<world.Packet.Packet>(data);
                    udpRecvAsync.SetBuffer(new byte[1024], 0, 1024);
                    udpSocket.ReceiveFromAsync(udpRecvAsync);
                    switch (p.packetType)
                    {
                        case (int)world.Packet.PacketType.OP_SessionCheck: sessionCheck(p.body,e.RemoteEndPoint);  break;
                        case (int)world.Packet.PacketType.OP_PlayerMove: broadCastMoveAtZone(data,p.body,p.session); break;
                        case (int)world.Packet.PacketType.OP_hit: broadCastHitAtZone(data, p.body, p.session); break;
                        case (int)world.Packet.PacketType.OP_kill: broadCastKillAtZone(data, p.body, p.session); break;
                    }
                }
                catch (JsonReaderException)
                {
                    Console.WriteLine("Exception");
                }
            }
            else
            {
                udpRecvAsync.SetBuffer(new byte[1024], 0, 1024);
                udpSocket.ReceiveFromAsync(udpRecvAsync);
            }
        }
        private void sessionCheck(string body,EndPoint e)
        {
            world.Packet.SessionCheck s = JsonConvert.DeserializeObject<world.Packet.SessionCheck>(body);
            int items=session.Session.session.FindIndex(x => x.key == s.session);
            try
            {
                byte[] buffer = new byte[1024];
                world.Packet.Packet p = new world.Packet.Packet((int)Packet.PacketType.OP_SessionCheckResult,false,"","");
                buffer = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(p));
                Console.WriteLine(e);
                session.Session.session[items].clientEp = e;
                sessionCheckReturn.RemoteEndPoint = e;
                sessionCheckReturn.SetBuffer(buffer, 0, buffer.Length);
                udpSocket.SendToAsync(sessionCheckReturn);
            }
            catch (ArgumentOutOfRangeException er)
            {
                Console.WriteLine(er);
                return;
            }
        }
        private void broadCastMoveAtZone(string packet,string body,string key)
        {
            world.Packet.MovementPacket m = JsonConvert.DeserializeObject<world.Packet.MovementPacket>(body);
            List<session.Session> list = session.Session.session.FindAll(x=>x.zone_id==m.zone_id);
            byte[] buffer = new byte[1024];
            buffer = Encoding.UTF8.GetBytes(packet);
            for (int i = 0; i < list.Count; i++)
            {
                if (key.Equals(list[i].key)) continue;
                try
                {
                    udpSocket.SendTo(buffer, list[i].clientEp);
                }
                catch(Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }
        private void broadCastHitAtZone(string packet,string body, string key)
        {
            world.Packet.HitInfo m = JsonConvert.DeserializeObject<world.Packet.HitInfo>(body);
            List<session.Session> list = session.Session.session.FindAll(x => x.zone_id == m.zone_id);
            byte[] buffer = new byte[1024];
            buffer = Encoding.UTF8.GetBytes(packet);
            for (int i = 0; i < list.Count; i++)
            {
                if (key.Equals(list[i].key)) continue;
                try
                {
                    udpSocket.SendTo(buffer, list[i].clientEp);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }
        private void broadCastKillAtZone(string packet, string body, string key)
        {
            world.Packet.KillInfo m = JsonConvert.DeserializeObject<world.Packet.KillInfo>(body);
            List<session.Session> list = session.Session.session.FindAll(x => x.zone_id == m.zone_id);
            byte[] buffer = new byte[1024];
            buffer = Encoding.UTF8.GetBytes(packet);
            for (int i = 0; i < list.Count; i++)
            {
                if (key.Equals(list[i].key)) continue;
                try
                {
                    udpSocket.SendTo(buffer, list[i].clientEp);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }
        public void broadcast_message(string message)
        {

        }
    }
}
